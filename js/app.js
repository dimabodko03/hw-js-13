const images = document.querySelectorAll('.image-to-show');
const stopBtn = document.getElementById('stop');
const resumeBtn = document.getElementById('resume');
let current = 0;
let intervalId = null;

function nextImg() {
    images[current].style.display = 'none';
    current = (current + 1) % images.length;
    images[current].style.display = 'block';
}

function startImg() {
    intervalId = setInterval(nextImg, 4000);
}

function stopImg() {
    clearInterval(intervalId);
}

stopBtn.addEventListener('click', () => {
    stopImg();
});

resumeBtn.addEventListener('click', () => {
    images[current].style.display = 'block';
    startImg();
});

for (let i = 1; i < images.length; i++) {
    images[i].style.display = 'none';
}

startImg();